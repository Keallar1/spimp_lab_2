# frozen_string_literal: true

class ThreadPool
  attr_accessor :queue, :size

  def initialize(size:)
    @threads = []
    @queue = Queue.new

    size.times do
      @threads << Thread.new(queue) do |queue|
        loop do
          task = queue.pop
          break if task == :shutdown

          task.call
        end
      end
    end
  end

  def perform(&task)
    queue.push(task)
  end

  def shutdown
    @threads.size.times { queue.push(:shutdown) }
    @threads.each(&:join)
  end
end

def search(target, arr, num_threads)
  pool = ThreadPool.new(size: num_threads)
  chunk_size = (arr.size / num_threads.to_f).ceil
  result = []

  num_threads.times do |i|
    start_index = i * chunk_size
    end_index = (i + 1) * chunk_size - 1

    pool.perform do
      result[i] = arr[start_index..end_index]&.index(target)&.send(:+, start_index)
    end
  end

  pool.shutdown
  result.compact.first
end

array = (1..100).to_a
element_to_find = 42
num_threads = 4

result = search(element_to_find, array, num_threads)
puts "Result of searching of element: #{result}"
