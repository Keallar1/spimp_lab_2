import java.util.concurrent.RecursiveTask;
import java.util.concurrent.ForkJoinPool;

class ParallelSearch extends RecursiveTask<Integer> {
    private int[] array;
    private int target;
    private int start;
    private int end;

    public ParallelSearch(int[] array, int target, int start, int end) {
        this.array = array;
        this.target = target;
        this.start = start;
        this.end = end;
    }

    @Override
    protected Integer compute() {
        if (end - start <= 1) {
            if (array[start] == target) {
                return start;
            } else {
                return -1;
            }
        }

        int mid = (start + end) / 2;

        ParallelSearch leftTask = new ParallelSearch(array, target, start, mid);
        ParallelSearch rightTask = new ParallelSearch(array, target, mid, end);

        leftTask.fork();
        int rightResult = rightTask.compute();
        int leftResult = leftTask.join();

        if (leftResult != -1) {
            return leftResult;
        } else {
            return rightResult;
        }
    }
}

public class Main {
    public static void main(String[] args) {
        int[] array = {1, 3, 5, 7, 9, 2, 4, 6, 8, 10, 11, 12, 13};
        int target = 0;

        ForkJoinPool forkJoinPool = new ForkJoinPool();
        ParallelSearch searchTask = new ParallelSearch(array, target, 0, array.length);
        int result = forkJoinPool.invoke(searchTask);

        if (result != -1) {
            System.out.println("Элемент " + target + " найден в позиции " + result + ".");
        } else {
            System.out.println("Элемент " + target + " не найден в массиве.");
        }
    }
}